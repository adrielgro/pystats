"""
WSGI config for pystats project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application

#path a donde esta el manage.py de nuestro proyecto Django
sys.path.append('/home/admin/groxor/django/pystats/')
#sys.path.append("/home/admin/groxor/django/pystats/pystats")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pystats.settings')

#prevenimos UnicodeEncodeError
os.environ.setdefault("LANG", "en_US.UTF-8")
os.environ.setdefault("LC_ALL", "en_US.UTF-8")

#activamos nuestro virtualenv
#activate_this = '/home/admin/groxor/virtualenvs/servstats/bin/activate'
#exec(open(activate_this, dict(__file__=activate_this)).read())

application = get_wsgi_application()
