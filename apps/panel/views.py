from django.http import HttpResponse
from django.template import loader
# import subprocess


# Create your views here.
def index(request):
    template = loader.get_template('panel/index.html')
    context = None

    # Leer archivos de datos
    storage_total           = int(open("/home/admin/groxor/django/pystats/scripts/data/storage_total.dat", "r").read())
    storage_available       = int(open("/home/admin/groxor/django/pystats/scripts/data/storage_avail.dat", "r").read())
    storage_administradores = int(open("/home/admin/groxor/django/pystats/scripts/data/storage_admin.dat", "r").read())
    storage_alumnos         = int(open("/home/admin/groxor/django/pystats/scripts/data/storage_alumn.dat", "r").read())
    storage_profesores      = int(open("/home/admin/groxor/django/pystats/scripts/data/storage_profe.dat", "r").read())
    storage_proyectos       = int(open("/home/admin/groxor/django/pystats/scripts/data/storage_proye.dat", "r").read())

    context = {
        "storage_total": storage_total,
        "storage_available":        round(storage_available/storage_total*100, 3),
        "storage_administradores":  round(storage_administradores/storage_total*100, 3),
        "storage_alumnos":          round(storage_alumnos/storage_total*100, 3),
        "storage_profesores":       round(storage_profesores/storage_total*100, 3),
        "storage_proyectos":        round(storage_proyectos/storage_total*100, 3),
    }

    return HttpResponse(template.render(context, request))